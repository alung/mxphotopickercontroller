//
//  ViewController.m
//  MXPhotoPickerDemo
//
//  Created by longminxiang on 13-8-23.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import "ViewController.h"
#import "MXPhotoPickerController.h"

@interface ViewController (){
    UIImageView *_imageView;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 40, 320, 320)];
    [self.view addSubview:_imageView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setFrame:CGRectMake(120, 400, 80, 44)];
    [button setTitle:@"touched" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(showPhotoPicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (void)showPhotoPicker:(id)sender{
    [self showMXPhotoPickerWithTitle:@"111" completion:^(UIImage *newImage, UIImage *originImage, CGRect cutRect) {
        [_imageView setFrame:CGRectMake(0, 20, 320, 320)];
        [_imageView setImage:newImage];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
