//
//  AppDelegate.h
//  MXPhotoPickerDemo
//
//  Created by longminxiang on 13-8-23.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
