//
//  MXPhotoPickerController.m
//  MaiDao
//
//  Created by along on 13-6-2.
//  Copyright (c) 2013年 along. All rights reserved.
//

#import "MXPhotoPickerController.h"
#import <QuartzCore/QuartzCore.h>

#define ISIOS7_MXPPC ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define EDIT_SIZE 320

#define TMPX 0.5

@interface UIImage (MXPhotoPickerController)

- (UIImage *)fixOrientation;

@end

@interface MXPhotoPickerController : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, copy) void (^doneBlock)(MXPhotoPickerController *pc, UIImage *newImage, CGRect rect, UIImage *originImage);

@end

@implementation MXPhotoPickerController

- (id)initWithImage:(UIImage *)image doneBlock:(void (^)(MXPhotoPickerController *pc, UIImage *newImage, CGRect rect, UIImage *originImage))block
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.doneBlock = [block copy];
        self.image = [image fixOrientation];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"选取" style:UIBarButtonItemStyleBordered target:self action:@selector(doneButtonTouched)]];
    if (ISIOS7_MXPPC) [self setAutomaticallyAdjustsScrollViewInsets:NO];

    CGRect frame = self.view.bounds;
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:frame];
    [scrollView setDelegate:self];
    [scrollView setMinimumZoomScale:1];
    [scrollView setMaximumZoomScale:5];
    
    CGSize imageSize = self.image.size;
    float imageViewHeight,imageViewWeight;
    float scale = imageSize.height / imageSize.width;
    if (scale > 1) {
        imageViewWeight = EDIT_SIZE;
        imageViewHeight = imageViewWeight * scale;
    }
    else {
        imageViewHeight = EDIT_SIZE;
        imageViewWeight = imageViewHeight / scale;
    }

    CGRect rect = CGRectMake(0, 0, imageViewWeight, imageViewHeight);
    self.imageView = [[UIImageView alloc] initWithFrame:rect];
    [self.imageView setImage:self.image];
    [scrollView addSubview:self.imageView];
    
    [scrollView setContentSize:CGSizeMake(imageViewWeight + TMPX, imageViewHeight+TMPX)];

    float hInset = ABS(scrollView.frame.size.height - EDIT_SIZE)/2;
    float wInset = ABS(scrollView.frame.size.width - EDIT_SIZE)/2;
    [scrollView setContentInset:UIEdgeInsetsMake(hInset, wInset, hInset, wInset)];
    
    float hInset2 = (imageViewHeight - scrollView.frame.size.height)/2;
    float wInset2 = (imageViewWeight - scrollView.frame.size.width)/2;
    [scrollView setContentOffset:CGPointMake(wInset2, hInset2)];
    
    [self.view addSubview:scrollView];
    
    frame.size.height = (frame.size.height - EDIT_SIZE) / 2;
    UIView *topView = [[UIView alloc] initWithFrame:frame];
    [topView setUserInteractionEnabled:NO];
    [topView setBackgroundColor:[UIColor blackColor]];
    [topView setAlpha:0.8];
    [self.view addSubview:topView];
    
    frame.origin.y += frame.size.height;
    frame.size.height = EDIT_SIZE;
    UIView *borderView = [[UIView alloc] initWithFrame:frame];
    [borderView setUserInteractionEnabled:NO];
    borderView.layer.borderWidth = 1;
    borderView.layer.borderColor = [[UIColor whiteColor] CGColor];
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:borderView.bounds];
    borderView.layer.shadowPath = path.CGPath;
    [self.view addSubview:borderView];
    
    frame.origin.y += frame.size.height;
    frame.size.height = self.view.bounds.size.height - frame.origin.y;
    UIView *bottomView = [[UIView alloc] initWithFrame:frame];
    [bottomView setUserInteractionEnabled:NO];
    [bottomView setBackgroundColor:[UIColor blackColor]];
    [bottomView setAlpha:0.8];
    [self.view addSubview:bottomView];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    CGSize contentSize = scrollView.contentSize;
    if (contentSize.width == EDIT_SIZE) {
        contentSize.width += TMPX;
        [scrollView setContentSize:contentSize];
    }
    if (contentSize.height == EDIT_SIZE) {
        contentSize.height += TMPX;
        [scrollView setContentSize:contentSize];
    }
    float hInset = ABS(scrollView.frame.size.height - EDIT_SIZE)/2;
    float wInset = ABS(scrollView.frame.size.width - EDIT_SIZE)/2;
    [scrollView setContentInset:UIEdgeInsetsMake(hInset, wInset, hInset, wInset)];
}

- (void)doneButtonTouched
{
    CGSize imgViewSize = self.imageView.frame.size;
    CGSize imgSize = self.image.size;
    float scale = imgSize.width / imgViewSize.width;
    
    float w = EDIT_SIZE * scale;
    float h = EDIT_SIZE * scale;
    
    UIScrollView *scrollView = (UIScrollView *)self.imageView.superview;
    CGPoint offset = scrollView.contentOffset;
    float x = offset.x * scale;
    float y = (offset.y + scrollView.contentInset.top) * scale;
    
    CGRect rect = CGRectMake(x, y, w, h);
    CGImageRef originImageRef = self.image.CGImage;
    CGImageRef subImageRef =CGImageCreateWithImageInRect(originImageRef,rect);
    UIImage *subImage = [UIImage imageWithCGImage:subImageRef];
    CFRelease(subImageRef);
    if (self.doneBlock) self.doneBlock (self, subImage, rect, self.image);
}

@end

#pragma mark === UIViewController Category ===
@interface UIViewController ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end

@implementation UIViewController (MXPhotoPicker)

static MXPhotoPickerBlock _pickerBlock;

- (void)showMXPhotoPickerWithTitle:(NSString *)title completion:(MXPhotoPickerBlock)completion
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"照相" otherButtonTitles:@"从相册中选择", nil];
    [actionSheet showInView:self.view];
    _pickerBlock = completion;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            NSLog(@"找不到相机");
            return ;
        }
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setDelegate:self];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker takePicture];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else if (buttonIndex == 1) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setDelegate:self];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    MXPhotoPickerController *ppcontroller = [[MXPhotoPickerController alloc] initWithImage:image doneBlock:^(MXPhotoPickerController *pc, UIImage *newImage, CGRect rect, UIImage *originImage) {
        [pc.navigationController dismissViewControllerAnimated:YES completion:nil];
        if (_pickerBlock) _pickerBlock(newImage, originImage, rect);
    }];
    [picker pushViewController:ppcontroller animated:YES];
    [picker setNavigationBarHidden:NO animated:YES];
}

@end

@implementation UIImage (MXPhotoPickerController)

- (UIImage *)fixOrientation
{
    UIImage *aImage = self;
    // No-op if the orientation is already correct
    if (aImage.imageOrientation == UIImageOrientationUp)
        return aImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,
                                             CGImageGetBitsPerComponent(aImage.CGImage), 0,
                                             CGImageGetColorSpace(aImage.CGImage),
                                             CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

@end
