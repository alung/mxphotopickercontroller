//
//  MXPhotoPickerController.h
//  MaiDao
//
//  Created by along on 13-6-2.
//  Copyright (c) 2013年 along. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (MXPhotoPicker)

typedef void (^MXPhotoPickerBlock)(UIImage *newImage, UIImage *originImage, CGRect cutRect);

- (void)showMXPhotoPickerWithTitle:(NSString *)title completion:(MXPhotoPickerBlock)completion;

@end